_start:
    addi r2, r0, 1
    addi r3, r0, 4000000
    addi r6, r0, 2
fib_loop:
    add r4, r1, r2
    addi r1, r2, 0
    addi r2, r4, 0
    rem r5, r4, r6
    beq r5, r0, cur_sum
    div r5, r4, r3
    beq r5, r0, fib_loop
    addi r3, r0, 10
    addi r1, r0, 99
    jmp push_stack
cur_sum:
    add r7, r7, r4
    jmp fib_loop
push_stack:
    rem r6, r7, r3
    st r6, sp
    subi sp, sp, 1
    div r7, r7, r3
    beq r7, r0, print_stack
    jmp push_stack
print_stack:
    addi sp, sp, 1
    ld r7, sp
    addi r7, r7, 48
    out r7
    beq sp, r1, done
    jmp print_stack
done:
      hlt