
- Зенкевич Артем Андреевич, P33081
- `asm | risc | neum | hw | instr | struct | stream | port | cstr | prob2`
- Упрощенный
## Язык программирования 
### EBNF

``` ebnf
program ::= {[label] [instruction] {newline | space}}
label ::= {symbol}':'
word ::= {symbol}
instruction ::= command [(register, register, register) | (register, register, number) |
                        (register, register, word) | (register, register) | number]

register ::= 'r0' | 'r1' | 'r2' | 'r3' | 'r4' | 'r5' | 'r6' | 'r7' | 'pc' | 'sp'

newline ::= \n
space :== \s
symbol ::= [a-zA-Z0-9_]
number ::= [0-9]+
command ::= 'ld' | 'st' | 'jmp' | 'beq' | 'add' | 'addi' | 'sub' | 'subi' |
            'mul' | 'div' | 'rem' | 'iret' | 'hlt' | 'in' | 'out'
```


Типы инструкций:

    A            # rd, rs1, rs2 - Register-register
    B            # rd, rs, imm - Immediate+
    C            # rd, rs - Register
    D            # imm - Immediate-only
    E            # rs1, rs2, imm - Register-immediate
    F            # no args
    G            # rd - Register-only

Инструкции:

    ld rd, rs                # type C        rd <- mem[rs] Загрузка значения из памяти. Значение, хранящееся в ячейке памяти с адресом, указанным в регистре rs, записывается в регистр rd.
    st rd, rs                # type C        rd -> mem[rs] Сохранение значения в память. Значение, содержащееся в регистре rd, записывается в ячейку памяти с адресом, указанным в регистре rs.

    jmp imm                  # type D        pc <- pc + imm Безусловный переход. Значение в регистре pc (счетчика команд) увеличивается на значение непосредственной константы imm, результат становится новым значением pc.
    beq rs1, rs2, imm        # type E        if rs1 == rs2:    pc <- pc + imm Условный переход. Если значения в регистрах rs1 и rs2 равны, то значение в регистре pc увеличивается на значение непосредственной константы imm, осуществляя переход.  

    add rd, rs1, rs2         # type A        rd <- rs1 + rs2   Сложение. Значение в регистре rd становится суммой значений в регистрах rs1 и rs2.
    addi rd, rs, imm         # type B        rd <- rs + imm Сложение с константой. Значение в регистре rd становится суммой значения в регистре rs и непосредственной константы imm.
    sub rd, rs1, rs2         # type A        rd <- rs1 - rs2 Вычитание. Значение в регистре rd становится разностью значений в регистрах rs1 и rs2.
    subi rd, rs, imm         # type B        rd <- rs - imm Вычитание константы. Значение в регистре rd становится разностью значения в регистре rs и непосредственной константы imm.
    mul rd, rs1, rs2         # type A        rd <- rs1 * rs2 Умножение. Значение в регистре rd становится произведением значений в регистрах rs1 и rs2.
    div rd, rs1, rs2         # type A        rd <- rs1 // rs2 Целочисленное деление. Значение в регистре rd становится частным от деления значения в регистре rs1 на значение в регистре rs2.
    rem rd, rs1, rs2         # type A        rd <- rs1 % rs2 Остаток от деления. Значение в регистре rd становится остатком от деления значения в регистре rs1 на значение в регистре rs2.

    in                       # type G        rd <- in Ввод. Значение из внешнего источника ввода записывается в регистр rd.
    out                      # type G        rd -> out Вывод. Значение в регистре rd передается на внешний источник вывода.

    iret                     # type F        Возврат из прерывания. Восстанавливает состояние процессора после обработки прерывания.
    hlt                      # type F        Остановка. Прекращает выполнение программы.


Код выполняется последовательно, начиная с метки _start.

Специальные метки:

    _start  точка входа
    _int    обработчик прерывания (ввод)

## Организация памяти

    ____________________ 
    | IRQ_HANDLER_ADDR |
    | ...              |
    | ...              |
    | STACK BEGINNING  |
    |__________________| 

Модель памяти процессора:

- Память общая для команд и данных. Каждая ячейка является либо словарем, описывающим инструкцию, либо числом. В начале находится адрес обработчика прерывания input. Предпоследний адрес соответствует устройству ввода, последний устройству вывода.

Типы адресации:

- Прямая регистровая: операндом инструкции является регистр.
- Непосредственная загрузка: одним из операндов является константа, подаваемая как один из аргументов.

## Особенности процессора

Интерфейс командной строки: `machine.py <file_code> <file_input>"`

- Машинное слово -- знаковое, 32 бита

- АЛУ:
    - на левый вход АЛУ вместо регистра может быть подана константа из инструкции;
    - АЛУ поддерживает операции: `ADD`, `SUB`, `MUL`, `DIV`, `REM`
- Регистры:
    - 7 регистров общего назначения (`R1`, `R2`, `R3`, `R4`, `R5`, `R6`, `R7`)
    - регистр `R0` всегда содержит значение 0
    - регистр, хранящий program counter, `PC`
    - регистр, указатель на вершину стека, `SP`
- Ввод-вывод:
    - port-mapped через специальные инструкции для ввода-вывода(`IN`, `OUT`).
- program_counter -- счётчик команд:
    - инкрементируется после каждой инструкции или перезаписывается инструкцией перехода.


### Кодирование инструкций

- Код ассемблера сериализуется в инструкции в формате JSON
```
{
    "start: 0,
    "code": [{"opcode": "addi", "rd": "r1", "rs": "r0", "imm": "104"}, ...]
}
```

где:
- start: точка входа
- code: инструкции

Типы данных в модуле isa, где:
- Opcode -- перечисление кодов операций;
- InstructionType -- типы инструкции;
- Register -- перечисление регистров процессора;

## Транслятор

Интерфейс командной строки: `translator.py <program.asm> <target>"`

Этапы трансляции (функция `translate`):

1. Разбиение текста на токены -- (label, [instr1, instr2, ...])
2. Установка токена _int в начало программы
2. Преобразование меток в адреса
3. Генерация машинного кода


## ControlUnit
![img_2.png](images%2Fimg_2.png)
Сигналы:
- -sel_next - мультиплексор на вход в Program Counter, либо инкремент текущего значения, либо CU_arg (поддержка прыжка по значению регистра)
- CU_arg - получается из мультиплексора, который управляется sel_arg. Либо arg из инструкции, либо вывод alu_output из datapath
- data_address - получается из мультиплексора, который управляется sel_mem_arg. Либо адрес из PC, либо CU_arg
- latch_pc - сохранить значение на входе в Program Counter
## DataPath
![img_2.png](images%2Fimg_1.png)
Реализован в классе `DataPath`.

- `memory` -- смешанная память
- `registers` -- регистры процессора
- `alu` -- АЛУ, выполняющее арифметические операции
- - `alu.op1` -- данные с левого входа АЛУ
- - `alu.op2` -- данные с правого входа АЛУ
- - `alu.opcode` -- установленный код операции
- - `alu.result` -- результат вычисления
- - `alu.ZF` -- zero флаг АЛУ, передается на CU по сигналу, используется для условных переходов
- `io_port` -- порт ввода-вывода
- - `input_buf` -- буфер с входными данными от внешнего устройства
- - `output_buf` -- буфер вывода.
- `mem_addr_bus` -- шина адреса в памяти, соединяется с выходом АЛУ

Сигналы:

- latch_data_addr - сохранить значение на входе в Data Address
- sel_reg_input - мультиплексор на входе в регистровый файл. Либо значение из памяти/устройства, либо control_unit_arg - может быть immediate value(а может быть значением другого регистра)
- -sel_lreg,  sel_rreg, sel_ireg - выбрать регистр (левый, правый или регистр, в который записывают)
- -sel_data - для выбора, что идет на alu
- -latch_ireg  - сохранить значение на входе в регистровый файл, в выбранный регистр для записи
- -alu_signals - выбор операции в ALU

Флаги:
- `Z` -- отражает наличие нулевого значения на выходе АЛУ. Используется для условных переходов.

## ControlUnit
Реализован в классе `ControlUnit`.

- Hardwired (реализовано полностью на python).
- Моделирование на уровне инструкций.
- Трансляция инструкции в последовательность сигналов: `decode_and_execute_instruction`.

Функции (наборы сигналов):

- `inc_program_counter` -- инкрементировать PC
- `pop_program_counter` -- записать в PC значение с вершины стека и инкрементировать SP
- `push_program_counter` -- декрементировать SP и записать значение PC на вершнину стека


Особенности работы модели:

- Для журнала состояний процессора используется стандартный модуль logging.
- Количество инструкций для моделирования ограничено параметром `limit`.
- Управление симуляцией реализовано в функции `simulation`.

#### Прерывания
- Система прерываний реализована через проверку наличия сигнала от ВУ в начале цикла выборки инструкции.
- Прерывания обслуживаются относительно: при поступлении сигнала прерывания во время нахождения в прерывании сигнал будет проигнорирован.
- В CU хранится адрес вектора прерывания.
- При прерывании по адресу `SP` сохраняется счетчик команд `PC`, `SP` декрементируется.
 Далее новое значение `PC` берется из памяти данных по адресу из вектора прерываний.

## Апробация

В качестве тестов использовано четыре алгоритма:

1. [hello world](tests/hello.asm).
2. [cat](tests/cat.asm) -- программа `cat`, повторяем ввод на выводе.
3. [prob2](tests/prob2.asm) -- рассчитать сумму четных чисел Фибоначи, меньших 40000

Голден-тесты: 
[golden](golden)

CL:
```yaml
lab3:
  stage: test
  image: python:latest
  script:
    - pip install pytest-golden // Установка библиотеки для голден тестов
    - pip install coverage // Установка библиотеки для прогона всех тестов
    - pip install mypy // Установка инструмента для статической проверки типов в коде Python
    - pip install pep8 // Установка инструмента для проверки соответствия кода Python стандартам стиля PEP 8
    - coverage run -m pytest --verbose
    - find . -type f -name "*.py" | xargs -t coverage report
    - find . -type f -name "*.py" | xargs -t mypy --check-untyped-defs
    - find . -type f -name "*.py" | xargs -t pep8 --ignore=E501,E701 // Проверки соответствия кода Python стандартам стиля PEP 8. E501 - строка больше 80 символов. E701 - множество инструкций на одной линии
```

Пример использования и журнал работы процессора на примере `cat`:

``` commandline
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 20 | instr_counter: 1 | tick: 4 | last_instr: {'opcode': <Opcode.JMP: 'jmp'>, 'imm': 0, 'type': <InstructionType.D: 'd'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 21 | instr_counter: 2 | tick: 7 | last_instr: {'opcode': <Opcode.IN: 'in'>, 'rd': 'r3', 'type': <InstructionType.G: 'g'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 22 | instr_counter: 3 | tick: 9 | last_instr: {'opcode': <Opcode.BEQ: 'beq'>, 'rs1': 'r3', 'rs2': 'r0', 'imm': 4, 'type': <InstructionType.E: 'e'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 23 | instr_counter: 4 | tick: 12 | last_instr: {'opcode': <Opcode.OUT: 'out'>, 'rd': 'r3', 'type': <InstructionType.G: 'g'>}
  DEBUG    root:machine.py:318 is_interrupted: False | PC: 24 | instr_counter: 5 | tick: 15 | last_instr: {'opcode': <Opcode.IRET: 'iret'>, 'type': <InstructionType.F: 'f'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 20 | instr_counter: 6 | tick: 19 | last_instr: {'opcode': <Opcode.JMP: 'jmp'>, 'imm': 0, 'type': <InstructionType.D: 'd'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 21 | instr_counter: 7 | tick: 22 | last_instr: {'opcode': <Opcode.IN: 'in'>, 'rd': 'r3', 'type': <InstructionType.G: 'g'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 22 | instr_counter: 8 | tick: 24 | last_instr: {'opcode': <Opcode.BEQ: 'beq'>, 'rs1': 'r3', 'rs2': 'r0', 'imm': 4, 'type': <InstructionType.E: 'e'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 23 | instr_counter: 9 | tick: 27 | last_instr: {'opcode': <Opcode.OUT: 'out'>, 'rd': 'r3', 'type': <InstructionType.G: 'g'>}
  DEBUG    root:machine.py:318 is_interrupted: False | PC: 24 | instr_counter: 10 | tick: 30 | last_instr: {'opcode': <Opcode.IRET: 'iret'>, 'type': <InstructionType.F: 'f'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 20 | instr_counter: 11 | tick: 34 | last_instr: {'opcode': <Opcode.JMP: 'jmp'>, 'imm': 0, 'type': <InstructionType.D: 'd'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 21 | instr_counter: 12 | tick: 37 | last_instr: {'opcode': <Opcode.IN: 'in'>, 'rd': 'r3', 'type': <InstructionType.G: 'g'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 22 | instr_counter: 13 | tick: 39 | last_instr: {'opcode': <Opcode.BEQ: 'beq'>, 'rs1': 'r3', 'rs2': 'r0', 'imm': 4, 'type': <InstructionType.E: 'e'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 23 | instr_counter: 14 | tick: 42 | last_instr: {'opcode': <Opcode.OUT: 'out'>, 'rd': 'r3', 'type': <InstructionType.G: 'g'>}
  DEBUG    root:machine.py:318 is_interrupted: False | PC: 24 | instr_counter: 15 | tick: 45 | last_instr: {'opcode': <Opcode.IRET: 'iret'>, 'type': <InstructionType.F: 'f'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 20 | instr_counter: 16 | tick: 49 | last_instr: {'opcode': <Opcode.JMP: 'jmp'>, 'imm': 0, 'type': <InstructionType.D: 'd'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 21 | instr_counter: 17 | tick: 52 | last_instr: {'opcode': <Opcode.IN: 'in'>, 'rd': 'r3', 'type': <InstructionType.G: 'g'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 22 | instr_counter: 18 | tick: 54 | last_instr: {'opcode': <Opcode.BEQ: 'beq'>, 'rs1': 'r3', 'rs2': 'r0', 'imm': 4, 'type': <InstructionType.E: 'e'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 23 | instr_counter: 19 | tick: 57 | last_instr: {'opcode': <Opcode.OUT: 'out'>, 'rd': 'r3', 'type': <InstructionType.G: 'g'>}
  DEBUG    root:machine.py:318 is_interrupted: False | PC: 24 | instr_counter: 20 | tick: 60 | last_instr: {'opcode': <Opcode.IRET: 'iret'>, 'type': <InstructionType.F: 'f'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 20 | instr_counter: 21 | tick: 64 | last_instr: {'opcode': <Opcode.JMP: 'jmp'>, 'imm': 0, 'type': <InstructionType.D: 'd'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 21 | instr_counter: 22 | tick: 67 | last_instr: {'opcode': <Opcode.IN: 'in'>, 'rd': 'r3', 'type': <InstructionType.G: 'g'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 22 | instr_counter: 23 | tick: 69 | last_instr: {'opcode': <Opcode.BEQ: 'beq'>, 'rs1': 'r3', 'rs2': 'r0', 'imm': 4, 'type': <InstructionType.E: 'e'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 23 | instr_counter: 24 | tick: 72 | last_instr: {'opcode': <Opcode.OUT: 'out'>, 'rd': 'r3', 'type': <InstructionType.G: 'g'>}
  DEBUG    root:machine.py:318 is_interrupted: False | PC: 24 | instr_counter: 25 | tick: 75 | last_instr: {'opcode': <Opcode.IRET: 'iret'>, 'type': <InstructionType.F: 'f'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 20 | instr_counter: 26 | tick: 79 | last_instr: {'opcode': <Opcode.JMP: 'jmp'>, 'imm': 0, 'type': <InstructionType.D: 'd'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 21 | instr_counter: 27 | tick: 82 | last_instr: {'opcode': <Opcode.IN: 'in'>, 'rd': 'r3', 'type': <InstructionType.G: 'g'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 22 | instr_counter: 28 | tick: 84 | last_instr: {'opcode': <Opcode.BEQ: 'beq'>, 'rs1': 'r3', 'rs2': 'r0', 'imm': 4, 'type': <InstructionType.E: 'e'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 23 | instr_counter: 29 | tick: 87 | last_instr: {'opcode': <Opcode.OUT: 'out'>, 'rd': 'r3', 'type': <InstructionType.G: 'g'>}
  DEBUG    root:machine.py:318 is_interrupted: False | PC: 24 | instr_counter: 30 | tick: 90 | last_instr: {'opcode': <Opcode.IRET: 'iret'>, 'type': <InstructionType.F: 'f'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 20 | instr_counter: 31 | tick: 94 | last_instr: {'opcode': <Opcode.JMP: 'jmp'>, 'imm': 0, 'type': <InstructionType.D: 'd'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 21 | instr_counter: 32 | tick: 97 | last_instr: {'opcode': <Opcode.IN: 'in'>, 'rd': 'r3', 'type': <InstructionType.G: 'g'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 22 | instr_counter: 33 | tick: 99 | last_instr: {'opcode': <Opcode.BEQ: 'beq'>, 'rs1': 'r3', 'rs2': 'r0', 'imm': 4, 'type': <InstructionType.E: 'e'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 23 | instr_counter: 34 | tick: 102 | last_instr: {'opcode': <Opcode.OUT: 'out'>, 'rd': 'r3', 'type': <InstructionType.G: 'g'>}
  DEBUG    root:machine.py:318 is_interrupted: False | PC: 24 | instr_counter: 35 | tick: 105 | last_instr: {'opcode': <Opcode.IRET: 'iret'>, 'type': <InstructionType.F: 'f'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 20 | instr_counter: 36 | tick: 109 | last_instr: {'opcode': <Opcode.JMP: 'jmp'>, 'imm': 0, 'type': <InstructionType.D: 'd'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 21 | instr_counter: 37 | tick: 112 | last_instr: {'opcode': <Opcode.IN: 'in'>, 'rd': 'r3', 'type': <InstructionType.G: 'g'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 22 | instr_counter: 38 | tick: 114 | last_instr: {'opcode': <Opcode.BEQ: 'beq'>, 'rs1': 'r3', 'rs2': 'r0', 'imm': 4, 'type': <InstructionType.E: 'e'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 23 | instr_counter: 39 | tick: 117 | last_instr: {'opcode': <Opcode.OUT: 'out'>, 'rd': 'r3', 'type': <InstructionType.G: 'g'>}
  DEBUG    root:machine.py:318 is_interrupted: False | PC: 24 | instr_counter: 40 | tick: 120 | last_instr: {'opcode': <Opcode.IRET: 'iret'>, 'type': <InstructionType.F: 'f'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 20 | instr_counter: 41 | tick: 124 | last_instr: {'opcode': <Opcode.JMP: 'jmp'>, 'imm': 0, 'type': <InstructionType.D: 'd'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 21 | instr_counter: 42 | tick: 127 | last_instr: {'opcode': <Opcode.IN: 'in'>, 'rd': 'r3', 'type': <InstructionType.G: 'g'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 22 | instr_counter: 43 | tick: 129 | last_instr: {'opcode': <Opcode.BEQ: 'beq'>, 'rs1': 'r3', 'rs2': 'r0', 'imm': 4, 'type': <InstructionType.E: 'e'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 23 | instr_counter: 44 | tick: 132 | last_instr: {'opcode': <Opcode.OUT: 'out'>, 'rd': 'r3', 'type': <InstructionType.G: 'g'>}
  DEBUG    root:machine.py:318 is_interrupted: False | PC: 24 | instr_counter: 45 | tick: 135 | last_instr: {'opcode': <Opcode.IRET: 'iret'>, 'type': <InstructionType.F: 'f'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 20 | instr_counter: 46 | tick: 139 | last_instr: {'opcode': <Opcode.JMP: 'jmp'>, 'imm': 0, 'type': <InstructionType.D: 'd'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 21 | instr_counter: 47 | tick: 142 | last_instr: {'opcode': <Opcode.IN: 'in'>, 'rd': 'r3', 'type': <InstructionType.G: 'g'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 22 | instr_counter: 48 | tick: 144 | last_instr: {'opcode': <Opcode.BEQ: 'beq'>, 'rs1': 'r3', 'rs2': 'r0', 'imm': 4, 'type': <InstructionType.E: 'e'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 23 | instr_counter: 49 | tick: 147 | last_instr: {'opcode': <Opcode.OUT: 'out'>, 'rd': 'r3', 'type': <InstructionType.G: 'g'>}
  DEBUG    root:machine.py:318 is_interrupted: False | PC: 24 | instr_counter: 50 | tick: 150 | last_instr: {'opcode': <Opcode.IRET: 'iret'>, 'type': <InstructionType.F: 'f'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 20 | instr_counter: 51 | tick: 154 | last_instr: {'opcode': <Opcode.JMP: 'jmp'>, 'imm': 0, 'type': <InstructionType.D: 'd'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 21 | instr_counter: 52 | tick: 157 | last_instr: {'opcode': <Opcode.IN: 'in'>, 'rd': 'r3', 'type': <InstructionType.G: 'g'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 22 | instr_counter: 53 | tick: 159 | last_instr: {'opcode': <Opcode.BEQ: 'beq'>, 'rs1': 'r3', 'rs2': 'r0', 'imm': 4, 'type': <InstructionType.E: 'e'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 23 | instr_counter: 54 | tick: 162 | last_instr: {'opcode': <Opcode.OUT: 'out'>, 'rd': 'r3', 'type': <InstructionType.G: 'g'>}
  DEBUG    root:machine.py:318 is_interrupted: False | PC: 24 | instr_counter: 55 | tick: 165 | last_instr: {'opcode': <Opcode.IRET: 'iret'>, 'type': <InstructionType.F: 'f'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 20 | instr_counter: 56 | tick: 169 | last_instr: {'opcode': <Opcode.JMP: 'jmp'>, 'imm': 0, 'type': <InstructionType.D: 'd'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 21 | instr_counter: 57 | tick: 172 | last_instr: {'opcode': <Opcode.IN: 'in'>, 'rd': 'r3', 'type': <InstructionType.G: 'g'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 22 | instr_counter: 58 | tick: 174 | last_instr: {'opcode': <Opcode.BEQ: 'beq'>, 'rs1': 'r3', 'rs2': 'r0', 'imm': 4, 'type': <InstructionType.E: 'e'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 23 | instr_counter: 59 | tick: 177 | last_instr: {'opcode': <Opcode.OUT: 'out'>, 'rd': 'r3', 'type': <InstructionType.G: 'g'>}
  DEBUG    root:machine.py:318 is_interrupted: False | PC: 24 | instr_counter: 60 | tick: 180 | last_instr: {'opcode': <Opcode.IRET: 'iret'>, 'type': <InstructionType.F: 'f'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 20 | instr_counter: 61 | tick: 184 | last_instr: {'opcode': <Opcode.JMP: 'jmp'>, 'imm': 0, 'type': <InstructionType.D: 'd'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 21 | instr_counter: 62 | tick: 187 | last_instr: {'opcode': <Opcode.IN: 'in'>, 'rd': 'r3', 'type': <InstructionType.G: 'g'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 22 | instr_counter: 63 | tick: 189 | last_instr: {'opcode': <Opcode.BEQ: 'beq'>, 'rs1': 'r3', 'rs2': 'r0', 'imm': 4, 'type': <InstructionType.E: 'e'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 23 | instr_counter: 64 | tick: 192 | last_instr: {'opcode': <Opcode.OUT: 'out'>, 'rd': 'r3', 'type': <InstructionType.G: 'g'>}
  DEBUG    root:machine.py:318 is_interrupted: False | PC: 24 | instr_counter: 65 | tick: 195 | last_instr: {'opcode': <Opcode.IRET: 'iret'>, 'type': <InstructionType.F: 'f'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 20 | instr_counter: 66 | tick: 199 | last_instr: {'opcode': <Opcode.JMP: 'jmp'>, 'imm': 0, 'type': <InstructionType.D: 'd'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 21 | instr_counter: 67 | tick: 202 | last_instr: {'opcode': <Opcode.IN: 'in'>, 'rd': 'r3', 'type': <InstructionType.G: 'g'>}
  DEBUG    root:machine.py:318 is_interrupted: True | PC: 25 | instr_counter: 68 | tick: 205 | last_instr: {'opcode': <Opcode.BEQ: 'beq'>, 'rs1': 'r3', 'rs2': 'r0', 'imm': 4, 'type': <InstructionType.E: 'e'>}
  DEBUG    root:machine.py:326 {<Register.R0: 'r0'>: 0, <Register.R1: 'r1'>: 0, <Register.R2: 'r2'>: 0, <Register.R3: 'r3'>: 0, <Register.R4: 'r4'>: 0, <Register.R5: 'r5'>: 0, <Register.R6: 'r6'>: 0, <Register.R7: 'r7'>: 0, <Register.PC: 'pc'>: 25, <Register.SP: 'sp'>: 72}
============================================================
  output: Hello, world!
  instr: 69  ticks: 206
```
Пример теста:
``` commandline
Testing started at 0:34 ...
Launching pytest with arguments integration_test.py::test_whole_by_golden --no-header --no-summary -q in C:\Users\Admin\PycharmProjects\csa-lab3-master

============================= test session starts =============================
collecting ... collected 3 items

integration_test.py::test_whole_by_golden[golden/cat.yml] 
integration_test.py::test_whole_by_golden[golden/hello.yml] 
integration_test.py::test_whole_by_golden[golden/prob2.yml] 

============================== 3 passed in 0.10s ==============================
PASSED         [ 33%]PASSED       [ 66%]PASSED       [100%]
Process finished with exit code 0
```
| ФИО            | алг.  | LoC | code инстр. | инстр.   | такт. |
|----------------|-------|-----|-------------|----------|-------|
| Зенкевич А. А. | hello | 12  | 11          | 11       | 31    |
| Зенкевич А. А. | cat   | 9   | 6           | 69       | 206   |
| Зенкевич А. А. | prob2 | 34  | 29          | 320      | 899   |

